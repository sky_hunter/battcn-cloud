package com.battcn.security.model.token;

public interface Token {
	String getToken();
}
